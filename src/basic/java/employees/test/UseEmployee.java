package basic.java.employees.test;

import basic.java.employees.Employee;

public class UseEmployee {

    public static void main(String[] args) {

        doTest1();

        doTest2();

        doTest3();
    }

    private static void doTest1() {

        System.out.println("In doTest1()...");

        /*
         * For this to work, you must define a default constructor in the Employee
         * class. Employee emp = new Employee();
         * 
         * emp.setName("Chris Smith"); emp.setSalary(20000); emp.setJoined(new Date());
         * 
         * emp.payRaise(500); emp.setName("Chris Jones");
         * 
         * System.out.println("Employee details: " + emp1);
         */
    }

    private static void doTest2() {

        System.out.println("\nIn doTest2()...");

        Employee emp1 = new Employee("John", 10000);
        System.out.println("emp1 details: " + emp1);

        Employee emp2 = new Employee("Jane", 20000);
        System.out.println("emp2 details: " + emp2);
    }

    private static void doTest3() {

        System.out.println("\nIn doTest3()...");

        // Create an employee with a salary below the minimum (should set salary to the
        // minimum, 8000).
        Employee emp1 = new Employee("John", 5000);
        System.out.println("emp1 details: " + emp1);

        // Create an employee with a salary above the minimum (should honour the
        // specified salary).
        Employee emp2 = new Employee("Jane", 10000);
        System.out.println("emp2 details: " + emp2);
    }
}
