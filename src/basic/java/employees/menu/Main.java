package basic.java.employees.menu;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

import basic.java.employees.Employee;

public class Main {

    public static void main(String[] args) {

        int id;
        double amount;
        Employee employee;
        Map<Integer, Employee> allEmployees = new HashMap<Integer, Employee>();

        // Display menu options in a loop.
        int option = -1;
        do {
            System.out.println();
            System.out.println("---------------------------------------------------------");
            System.out.println("1. Hire employee");
            System.out.println("2. Fire employee");
            System.out.println("3. Give employee a pay rise");
            System.out.println("4. Get an employee");
            System.out.println("5. Get all employees");
            System.out.println("6. Quit");

            try {
                option = CmdLineScanner.getInt("\nEnter option => ");

                switch (option) {

                case 1:
                    employee = new Employee(CmdLineScanner.getString("\nEnter employee name: "));
                    allEmployees.put(employee.getId(), employee);
                    System.out.println("Hired: " + employee + "\n");
                    break;

                case 2:
                    id = CmdLineScanner.getInt("Enter id: ");
                    allEmployees.remove(id);
                    System.out.println("Fired employee with id " + id + "\n");
                    break;

                case 3:
                    id = CmdLineScanner.getInt("Enter id: ");
                    amount = CmdLineScanner.getDouble("Enter dollar amount: ");
                    employee = allEmployees.get(id);
                    if(employee != null) {
                        employee.payRaise(amount);
                        System.out.printf("Given $%.2f pay rise to employee:\n", amount, employee);
                    }
                    else {
                        System.out.printf("No employee with id %s.\n", id);
                    }
                    break;

                case 4:
                    id = CmdLineScanner.getInt("Enter id: ");
                    employee = allEmployees.get(id);
                    if (employee != null) {
                        System.out.printf("Employee details: %s.\n", employee);
                    } else {
                        System.out.printf("No employee with id %s.\n", id);
                    }
                    break;

                case 5:
                    System.out.println("All employees");
                    CmdLineScanner.displayCollection(allEmployees);
                    break;

                case 6:
                    // This is a valid option, but there's nothing to do here.
                    break;

                default:
                    System.out.println("Invalid option selected.");
                }
            } catch(InputMismatchException ex) {
                System.out.println("Invalid option selected - Exiting");
                option = 6;
            }
        } while (option != 6);
    }
}
