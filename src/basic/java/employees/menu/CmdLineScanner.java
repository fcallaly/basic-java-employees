package basic.java.employees.menu;

import java.util.Map;
import java.util.Scanner;

// This class comprises static helper methods.
public class CmdLineScanner {

	// Create a Scanner object, to get keyboard input.
	private static Scanner scanner = new Scanner(System.in);
	
	// Get a String from the user.
	public static String getString(String promptMsg) {
		System.out.printf("%s", promptMsg);
		return scanner.next();
	}
	
	// Get a double from the user.
	public static double getDouble(String promptMsg) {
		System.out.printf("%s", promptMsg);
		return scanner.nextDouble();
	}
	
	// Get an int from the user.
	public static int getInt(String promptMsg) {
		System.out.printf("%s", promptMsg);
		return scanner.nextInt();
	}
	
	// Generic method, displays all the items in a Collection<T>.
	public static <K, V> void displayCollection(Map<K, V> map) {
		for (V element : map.values()) {
			System.out.printf("%s.\n", element);
		}
	}
}
